(function($) {

  $(document).ready(function() {
      var intervalID;
       intervalID  = setInterval(function() {
           openerp.jsonRpc('/page/flixo_svg_calculate_json', 'call', {token:$('.svg_wrap_result .svg_data .token').text()}).then(function(data){
              if (data['error'] == 'True') {
                  $('.notice_error_post .text').html(data['error_message']);
                  show_notification('.notice_error_post', '.svg_wrap');
                  //clearInterval(intervalID);
              }
              if (data['error'] == 'False') {
                  if (data['link'] != '') {
                      window.location = data['link'];
                      // clearInterval(intervalID);
                  }
              }
           },"json");
          }, 30000);
    openerp.jsonRpc('/page/flixo_svg_calculate_json', 'call', {token:$('.svg_wrap_result .svg_data .token').text()}).then(function(data) {
    //$.getJSON('/ajax/calculate_json.php', { }, function(data) {

      if (typeof data === 'object') {

          /*$('.svg_wrap_result .param_q').html(data.q.toFixed(4));
          $('.svg_wrap_result .param_up1_bp1').html(' - '+data.up1.toFixed(4)+'*'+data.bp1.toFixed(4));
          $('.svg_wrap_result .param_value').html(data.Uf.toFixed(4)+' W/(m<sup>2</sup> * K)');
          $('.svg_wrap_result .param_dT').html(data.dt);
          $('.svg_wrap_result .param_bf').html(data.bf);
          */

        $('.muntin-params').hide();

        if (typeof data.frame !== 'undefined') {
          if (data.frame === 'external') {//single
            $('#calc-single-wing').attr('checked', 'checked');
            $('.calc-inner-frame').hide();
          } else if (data.frame === 'internal') {//double
            $('#calc-double-wing').attr('checked', 'checked');
          }
        }

        if (typeof data.glass_data !== 'undefined' && typeof data.glass_info !== 'undefined') {
          if (typeof data.glass_data.double !== 'undefined' && typeof data.glass_info.double !== 'undefined') {
            $('#calc-double-glazing').attr('checked', 'checked');
            $('#calc-dimensions, #calc-gas-type, #calc-glass-material').empty();
            $.each(data.glass_info.double.dimensions, function(i, v) {
              $('#calc-dimensions').append('<option value="'+ i +'">'+ v +'</option>');
            });
            $.each(data.glass_info.double.gas_type, function(i, v) {
              $('#calc-gas-type').append('<option value="'+ i +'">'+ v +'</option>');
            });
            $.each(data.glass_info.double.material, function(i, v) {
              $('#calc-glass-material').append('<option value="'+ i +'">'+ v +'</option>');
            });
            var dimensions = $('#calc-dimensions').find('option:selected').val();
            var gasType = $('#calc-gas-type').find('option:selected').val();
            var material = $('#calc-glass-material').find('option:selected').val();
            $('#calc-ug').val(data.glass_data.double[material][dimensions][gasType])
          }
        }

        if (typeof data.Wg !== 'undefined') {
          $.each(data.Wg, function(i, v) {
            $('#calc-spacer-material').append('<option value="'+ v.value+'">'+ v.title+'</option>');
            if (i == 0) {
              $('#calc-wg').val(v.value);
            }
          });
        }

        if (typeof data.bf !== 'undefined') {
          $('#calc-main-frame-width').val(data.bf);
        }

        if (typeof data.Uf !== 'undefined') {
          $('#calc-main-frame-uf').val(data.Uf);
        }

        var calculateWindow = {
          updateUg: function() {
            var glass, dimensions, material, gasType;
            if ($('#calc-double-glazing').is(':checked')) {
              glass = 'double';
            } else if ($('#calc-triple-glazing').is(':checked')) {
              glass = 'triple';
            }
            dimensions = $('#calc-dimensions').find('option:selected').val();
            gasType = $('#calc-gas-type').find('option:selected').val();
            material = $('#calc-glass-material').find('option:selected').val();
            $('#calc-ug').val(data.glass_data[glass][material][dimensions][gasType]);
          },
          updateGlass: function() {
            var glass;
            if ($('#calc-double-glazing').is(':checked')) {
              glass = 'double';
            } else if ($('#calc-triple-glazing').is(':checked')) {
              glass = 'triple';
            }
            $('#calc-dimensions, #calc-gas-type, #calc-glass-material').empty();
            $.each(data.glass_info[glass].dimensions, function(i, v) {
              $('#calc-dimensions').append('<option value="'+ i +'">'+ v +'</option>');
            });
            $.each(data.glass_info[glass].gas_type, function(i, v) {
              $('#calc-gas-type').append('<option value="'+ i +'">'+ v +'</option>');
            });
            $.each(data.glass_info[glass].material, function(i, v) {
              $('#calc-glass-material').append('<option value="'+ i +'">'+ v +'</option>');
            });
            this.updateUg();
          },
          updateMuntinMaterialChange: function() {
            if ($('#calc-muntin-bars-material').find('option:selected').attr('data-type') == 'fixed') {
              $('.calc-muntin-bars-amount').hide();
            } else {
              $('.calc-muntin-bars-amount').show();
            }
          },
          updateMuntinTypeChange: function() {
            if ($('.calc-muntin-bars-type:checked').val() == 'without_muntins') {
              $('.muntin-params').hide();
            } else if ($('.calc-muntin-bars-type:checked').val() == 'window_muntins') {
              if (typeof data.Ws_window !== 'undefined') {
                $('#calc-muntin-bars-material').empty();
                $.each(data.Ws_window, function(i, v) {
                  $('#calc-muntin-bars-material').append('<option data-type="'+ v.type+'" value="'+ v.value+'">'+ v.title+'</option>');
                  if (i == 0) {
                    $('#calc-muntin-ws').val(v.value);
                  }
                });
                $('.muntin-params').show();
              }
              this.updateMuntinMaterialChange();
            } else if ($('.calc-muntin-bars-type:checked').val() == 'internal_muntins') {
              if (typeof data.Ws_internal !== 'undefined') {
                $('#calc-muntin-bars-material').empty();
                $.each(data.Ws_internal, function(i, v) {
                  $('#calc-muntin-bars-material').append('<option data-type="'+ v.type+'" value="'+ v.value+'">'+ v.title+'</option>');
                  if (i == 0) {
                    $('#calc-muntin-ws').val(v.value);
                  }
                });
                $('.muntin-params').show();
              }
              if ($('#calc-muntin-bars-material').find('option:selected').attr('data-type') == 'fixed') {
                $('.calc-muntin-bars-amount').hide();
              } else {
                $('.calc-muntin-bars-amount').show();
              }
              this.updateMuntinMaterialChange();
            }
          },
          getFormula: function(frame) {
            if (frame == 'external') { //single
              var fixedAdd;
              if ($('.calc-muntin-bars-type:checked').val() == 'without_muntins') {
                return 'U<sub>w</sub> = <div class="formula-del">' +
                                          '<span class="formula-top">A<sub>g</sub>U<sub>g</sub> + A<sub>f</sub>U<sub>f</sub> + l<sub>g</sub>Ψ<sub>g</sub></span>' +
                                          '<span class="formula-bottom">A<sub>g</sub> + A<sub>f</sub></span>' +
                                        '</div>';
              } else if ($('#calc-muntin-bars-material').find('option:selected').attr('data-type') == 'fixed') {
                fixedAdd = parseFloat($('#calc-muntin-ws').val());
                return 'U<sub>w</sub> = <div class="formula-del">' +
                                          '<span class="formula-top">A<sub>g</sub>U<sub>g</sub> + A<sub>f</sub>U<sub>f</sub> + l<sub>g</sub>Ψ<sub>g</sub></span>' +
                                          '<span class="formula-bottom">A<sub>g</sub> + A<sub>f</sub></span>' +
                                        '</div> + ' + fixedAdd;
              } else if ($('#calc-muntin-bars-material').find('option:selected').attr('data-type') == 'material') {
                return 'U<sub>w</sub> = <div class="formula-del">' +
                                          '<span class="formula-top">A<sub>g</sub>U<sub>g</sub> + A<sub>f</sub>U<sub>f</sub> + l<sub>g</sub>Ψ<sub>g</sub> + l<sub>s</sub>Ψ<sub>s</sub></span>' +
                                          '<span class="formula-bottom">A<sub>g</sub> + A<sub>f</sub></span>' +
                                        '</div>';
              }

            } else if (frame == 'internal') { //double
              if ($('.calc-muntin-bars-type:checked').val() == 'without_muntins') {
                return 'U<sub>w</sub> = <div class="formula-del">' +
                                          '<span class="formula-top">A<sub>g</sub>U<sub>g</sub> + A<sub>f</sub>U<sub>f</sub> + l<sub>g</sub>Ψ<sub>g</sub> + A<sub>f-i</sub>U<sub>f-i</sub></span>' +
                                          '<span class="formula-bottom">A<sub>g</sub> + A<sub>f</sub> + A<sub>f-i</sub></span>' +
                                        '</div>';
              } else if ($('#calc-muntin-bars-material').find('option:selected').attr('data-type') == 'fixed') {
                fixedAdd = parseFloat($('#calc-muntin-ws').val());
                return 'U<sub>w</sub> = <div class="formula-del">' +
                                          '<span class="formula-top">A<sub>g</sub>U<sub>g</sub> + A<sub>f</sub>U<sub>f</sub> + l<sub>g</sub>Ψ<sub>g</sub> + A<sub>f-i</sub>U<sub>f-i</sub></span>' +
                                          '<span class="formula-bottom">A<sub>g</sub> + A<sub>f</sub> + A<sub>f-i</sub></span>' +
                                        '</div> + ' + fixedAdd;
              } else if ($('#calc-muntin-bars-material').find('option:selected').attr('data-type') == 'material') {
                return 'U<sub>w</sub> = <div class="formula-del">' +
                                          '<span class="formula-top">A<sub>g</sub>U<sub>g</sub> + A<sub>f</sub>U<sub>f</sub> + l<sub>g</sub>Ψ<sub>g</sub> + A<sub>f-i</sub>U<sub>f-i</sub> + l<sub>s</sub>Ψ<sub>s</sub></span>' +
                                          '<span class="formula-bottom">A<sub>g</sub> + A<sub>f</sub> + A<sub>f-i</sub></span>' +
                                        '</div>';
              }
            }
          },
          insertFormulaPart: function(className, valueInsert) {
            $('#calculate-info-right').find('.calc-main-info .'+className+' .data').html(valueInsert);
          },
          getCalcValue: function(frame) {

            var Uw = 0;
            var Ag, Af, lg, fixedAdd, Ws, horMuntins, verMuntins, ls;
            var w = parseFloat($('#calc-window-width').val());
            var h = parseFloat($('#calc-window-height').val());
            var Ug = parseFloat($('#calc-ug').val());
            var Wg = parseFloat($('#calc-wg').val());
            var bf = parseFloat($('#calc-main-frame-width').val());
            var Uf = parseFloat($('#calc-main-frame-uf').val());

            if (frame == 'external') { //single

              Ag = (h - 2 * bf) * (w - 2 * bf);
              Af = w * h - (h - 2 * bf) * (w - 2 * bf);
              lg = 2 * (h - 2 * bf + w - 2 * bf);
              this.insertFormulaPart('Ag', Ag.toFixed(3));
              this.insertFormulaPart('Af', Af.toFixed(3));
              this.insertFormulaPart('lg', lg.toFixed(3));
              $('#calculate-info-right').find('.calc-main-info .Afi').hide();

              if ($('.calc-muntin-bars-type:checked').val() == 'without_muntins') {
                $('#calculate-info-right').find('.calc-main-info .ls').hide();
                Uw = (Ag * Ug + Af * Uf + lg * Wg) / (Ag + Af);
              } else if ($('#calc-muntin-bars-material').find('option:selected').attr('data-type') == 'fixed') {
                $('#calculate-info-right').find('.calc-main-info .ls').hide();
                fixedAdd = parseFloat($('#calc-muntin-ws').val());
                Uw = (Ag * Ug + Af * Uf + lg * Wg) / (Ag + Af) + fixedAdd;
              } else if ($('#calc-muntin-bars-material').find('option:selected').attr('data-type') == 'material') {
                Ws = parseFloat($('#calc-muntin-ws').val());
                horMuntins = parseFloat($('#calc-muntin-bars-horizontal-amount').val());
                verMuntins = parseFloat($('#calc-muntin-bars-vertical-amount').val());
                ls = 2 * (w * horMuntins + h * verMuntins);
                this.insertFormulaPart('ls', ls.toFixed(3));
                $('#calculate-info-right').find('.calc-main-info .ls').show();
                Uw = (Ag * Ug + Af * Uf + lg * Wg + ls * Ws) / (Ag + Af);
              }
              return Uw.toFixed(3);

            } else if (frame == 'internal') { //double

              var bfI = parseFloat($('#calc-inner-frame-width').val());
              var Ufi = parseFloat($('#calc-inner-frame-uf').val());
              var Afi = (h - 2 * bf) * bfI + bfI * bf;
              Ag = (h - 2 * bf) * (w - 2 * bf - bfI);
              Af = h * w - Ag - Afi;
              lg = 2 * (h - 2 * bf + w - 2 * bf - 2 * bfI);
              this.insertFormulaPart('Ag', Ag.toFixed(3));
              this.insertFormulaPart('Af', Af.toFixed(3));
              this.insertFormulaPart('lg', lg.toFixed(3));
              this.insertFormulaPart('Afi', Afi.toFixed(3));
              $('#calculate-info-right').find('.calc-main-info .Afi').show();

              if ($('.calc-muntin-bars-type:checked').val() == 'without_muntins') {
                $('#calculate-info-right').find('.calc-main-info .ls').hide();
                Uw = (Ag * Ug + Af * Uf + lg * Wg + Afi * Ufi) / (Ag + Af + Afi);
              } else if ($('#calc-muntin-bars-material').find('option:selected').attr('data-type') == 'fixed') {
                $('#calculate-info-right').find('.calc-main-info .ls').hide();
                fixedAdd = parseFloat($('#calc-muntin-ws').val());
                Uw = (Ag * Ug + Af * Uf + lg * Wg + Afi * Ufi) / (Ag + Af + Afi) + fixedAdd;
              } else if ($('#calc-muntin-bars-material').find('option:selected').attr('data-type') == 'material') {
                Ws = parseFloat($('#calc-muntin-ws').val());
                horMuntins = parseFloat($('#calc-muntin-bars-horizontal-amount').val());
                verMuntins = parseFloat($('#calc-muntin-bars-vertical-amount').val());
                ls = 2 * (w * horMuntins + 2 * h * verMuntins);
                this.insertFormulaPart('ls', ls.toFixed(3));
                $('#calculate-info-right').find('.calc-main-info .ls').show();
                Uw = (Ag * Ug + Af * Uf + lg * Wg + Afi * Ufi + ls * Ws) / (Ag + Af + Afi);
              }
              return Uw.toFixed(3);

            }
          },
          updateImage: function(imageName, insertPlace) {
            var $imageContainer = $('#calculate-info-right').find('.calc-images-container');
              //TODO
              var imageBasePath = '/flixo_online/static/images/calculate/';
            //var imageBasePath = '/thermevo_website_management/static/src/images/calculate/';
            var imagePath = imageBasePath + imageName;
            if (insertPlace == 'prepend') {
              if ($imageContainer.find('img.left').length > 0) {
                if ($imageContainer.find('img.left').attr('data-image-name') != imageName) {
                  $imageContainer.find('img.left').remove();
                  $imageContainer.prepend('<img class="left" src="' + imagePath + '" alt="" data-image-name="' + imageName + '">');
                }
              } else {
                $imageContainer.prepend('<img class="left" src="' + imagePath + '" alt="" data-image-name="' + imageName + '">');
              }
            } else if (insertPlace == 'append') {
              if ($imageContainer.find('img.right').length > 0) {
                if ($imageContainer.find('img.right').attr('data-image-name') != imageName) {
                  $imageContainer.find('img.right').remove();
                  $imageContainer.append('<img class="right" src="' + imagePath + '" alt="" data-image-name="' + imageName + '">');
                }
              } else {
                $imageContainer.append('<img class="right" src="' + imagePath + '" alt="" data-image-name="' + imageName + '">');
              }
            }
          },
          insertImages: function(frame, glass) {
            var horMuntins = parseFloat($('#calc-muntin-bars-horizontal-amount').val());
            var verMuntins = parseFloat($('#calc-muntin-bars-vertical-amount').val());
            if (frame == 'external') {
              if ($('.calc-muntin-bars-type:checked').val() == 'without_muntins') {
                this.updateImage('calc_image1.jpg', 'prepend');
              } else if ( horMuntins >= 1 && verMuntins >= 1 ) {
                this.updateImage('calc_image1_vermun_1_hormun_1.jpg', 'prepend');
              } else if ( horMuntins >= 1 && verMuntins < 1 ) {
                this.updateImage('calc_image1_vermun_0_hormun_1.jpg', 'prepend');
              } else if ( horMuntins < 1 && verMuntins >= 1 ) {
                this.updateImage('calc_image1_vermun_1_hormun_0.jpg', 'prepend');
              } else if ( horMuntins < 1 && verMuntins < 1 ) {
                this.updateImage('calc_image1.jpg', 'prepend');
              }
            } else if (frame == 'internal') {
              if ($('.calc-muntin-bars-type:checked').val() == 'without_muntins') {
                this.updateImage('calc_image2.jpg', 'prepend');
              } else if ( horMuntins >= 1 && verMuntins >= 1 ) {
                this.updateImage('calc_image2_vermun_1_hormun_1.jpg', 'prepend');
              } else if ( horMuntins >= 1 && verMuntins < 1 ) {
                this.updateImage('calc_image2_vermun_0_hormun_1.jpg', 'prepend');
              } else if ( horMuntins < 1 && verMuntins >= 1 ) {
                this.updateImage('calc_image2_vermun_1_hormun_0.jpg', 'prepend');
              } else if ( horMuntins < 1 && verMuntins < 1 ) {
                this.updateImage('calc_image2.jpg', 'prepend');
              }
            }
            if (glass == 'double') {
              if ($('.calc-muntin-bars-type:checked').val() == 'without_muntins') {
                this.updateImage('duplex_0.png', 'append');
              } else if ($('.calc-muntin-bars-type:checked').val() == 'window_muntins') {
                this.updateImage('duplex_2.png', 'append');
              } else if ($('.calc-muntin-bars-type:checked').val() == 'internal_muntins') {
                this.updateImage('duplex_1.png', 'append');
              }
            } else if (glass == 'triple') {
              if ($('.calc-muntin-bars-type:checked').val() == 'without_muntins') {
                this.updateImage('tryplex_0.png', 'append');
              } else if ($('.calc-muntin-bars-type:checked').val() == 'window_muntins') {
                this.updateImage('tryplex_2.png', 'append');
              } else if ($('.calc-muntin-bars-type:checked').val() == 'internal_muntins') {
                this.updateImage('tryplex_1.png', 'append');
              }
            }
          },
          insertFormula: function(frame) {
            var formula = this.getFormula(frame);
            var value = this.getCalcValue(frame);
            $('#calculate-info-right').find('.calc-main-info .formula').html(formula + ' = ' + value);
          },
          change: function() {
            var frame, glass;
            if ($('#calc-single-wing').is(':checked')) {
              frame = 'external';
            } else if ($('#calc-double-wing').is(':checked')) {
              frame = 'internal';
            }
            if ($('#calc-double-glazing').is(':checked')) {
              glass = 'double';
            } else if ($('#calc-triple-glazing').is(':checked')) {
              glass = 'triple';
            }
            this.insertImages(frame, glass);
            this.insertFormula(frame);
          }
        };

        calculateWindow.change();

        $('.calc-window-type-input').on('change', function() {
          if ($(this).attr('id') == 'calc-single-wing') {
            $('.calc-inner-frame').hide();
          } else if ($(this).attr('id') == 'calc-double-wing') {
            $('.calc-inner-frame').show();
          }
          calculateWindow.change();
        });

        $('.calc-glazing-input').on('change', function() {
          calculateWindow.updateGlass();
          calculateWindow.change();
        });

        $('#calc-dimensions, #calc-gas-type, #calc-glass-material').on('change', function() {
          calculateWindow.updateUg();
          calculateWindow.change();
        });

        $('#calc-spacer-material').on('change', function() {
          var newVal = $(this).val();
          $('#calc-wg').val(newVal);
          calculateWindow.change();
        });

        $('.calc-muntin-bars-type').on('change', function() {
          calculateWindow.updateMuntinTypeChange();
          calculateWindow.change();
        });

        $('#calc-muntin-bars-material').on('change', function() {
          var newVal = $(this).val();
          $('#calc-muntin-ws').val(newVal);
          calculateWindow.updateMuntinMaterialChange();
          calculateWindow.change();
        });

        $('#calc-window-width, #calc-window-height, #calc-wg, #calc-ug, #calc-main-frame-width, #calc-main-frame-uf, #calc-inner-frame-width, #calc-inner-frame-uf, #calc-muntin-ws').on('change', function() {
          calculateWindow.change();
        });

        $('#calc-muntin-bars-horizontal-amount, #calc-muntin-bars-vertical-amount').on('change', function() {
          if (parseInt($(this).val()) > 5) {
            $(this).val(5);
          }
          calculateWindow.change();
        });

        $(".form-number").on('keydown', function (e) {
          // Allow: backspace, delete, tab, escape, enter and .
          if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
              // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) ||
              // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
          }
          // Ensure that it is a number and stop the keypress
          if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
          }
        });

        $('.calc-form-inc').on('click', function() {
          var $input = $(this).closest('.form-item').find('input');
          var val = parseFloat($input.val());
          var inc = parseFloat($(this).attr('data-inc'));
          var fixed = parseFloat($(this).attr('data-fixed'));
          if ( ($input.attr('id') == 'calc-muntin-bars-horizontal-amount' || $input.attr('id') == 'calc-muntin-bars-vertical-amount') && (val + inc > 5) ) {
          } else {
            $input.val( (val + inc).toFixed(fixed) );
          }
          calculateWindow.change();
        });

        $('.calc-form-dec').on('click', function() {
          var $input = $(this).closest('.form-item').find('input');
          var val = parseFloat($input.val());
          var dec = parseFloat($(this).attr('data-dec'));
          var fixed = parseFloat($(this).attr('data-fixed'));
          if ( parseFloat(val - dec) >= 0 ) {
            $input.val( (val - dec).toFixed(fixed) );
            calculateWindow.change();
          }
        });

      } else {
        console.log('Incorrect data');
      }

    });

  });

})(jQuery);
