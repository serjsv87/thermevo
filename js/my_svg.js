var default_width = '';
var default_height = '';
var default_viewbox = '';
var groups = [];
var materials = [];
var current_group = 0;
var scrollpane ="";
var polygonsArr = [];
var temperature = [];
var current_temperature = '';
var red_temperature = '';
var blue_temperature = '';
var svgPanZoom = '';
var window_count = 0;
var list_material= 'none';
var list_material_array= [];

$.expr[':'].icontains = function(a, i, m) {
    return $(a).text().toUpperCase()
        .indexOf(m[3].toUpperCase()) >= 0;
};

function rebuild_jscroll () {
    if ($(window).width() < 960) {
        $('.material_list').height(108);
    } else {
        $('.material_list').height($('.svg_wrap #svg').height()-$('.buttons_top').outerHeight()-$('.material_list_top').outerHeight()-38);
    }
}

function append_created(name_user,color,lambda,eps,dencity,material_id) {

    var name = '';
    var output = '';

    if (lambda != '') {
        name += 'λ = ' + lambda;
    }

    if (eps != '') {
        if (name != '') {
            name += ', ';
        }
        name  += 'ε = ' + eps;
    }
    if (dencity != '') {
        if (name != '') {
            name += ', ';
        }
        name  += 'd = ' + dencity;
    }
    name += ' <br> ' + name_user;

    output += '<div class="material_item material' + material_id + '" material="' + material_id +'" onclick="fill_path_by_id(\'' + material_id +'\');">';
    output += '<div class="color_name">';
    output += '<span class="color_block" style="background:' + color +'"><span class="top_triangle"></span><span class="bottom_triangle"></span></span>';
    output += '<span class="name">'+ name + '</span>';
    output += '</div>';
    output += '</div>';

    materials[material_id]=[];
    materials[material_id][0]=material_id;
    materials[material_id][1]=name_user;
    materials[material_id][2]=dencity;
    materials[material_id][3]=lambda;
    materials[material_id][4]=eps;
    materials[material_id][5]=color;

    return output;
}

function processData(allText) {
    var output = "";
    var tarr = allText[0];
    $('.item_group_block_change .button_air').html('<div class="change_to_air" onclick="fill_path_by_id(\'' + tarr[0] +'\');">' + tarr[1] + '</div>').attr('material',tarr[0]);
    for (var i=0; i<allText.length; i++) {
        var name = "";
        tarr = allText[i];

        materials[tarr[0]] = [];
        materials[tarr[0]] = tarr;

        if (tarr[3] != '') {
            name += 'λ = ' + tarr[3];
        }

        if (tarr[4] != '') {
            if (name != '') {
                name += ', ';
            }
            name  += 'ε = ' + tarr[4];
        }
        if (tarr[2] != '') {
            if (name != '') {
                name += ', ';
            }
            name  += 'd = ' + tarr[2];
        }
        name += ' <br> ' + tarr[1];
        if (i != 0) {
            output += '<div class="material_item material' + tarr[0] + '" material="' + tarr[0] +'" onclick="fill_path_by_id(\'' + tarr[0] +'\');">';
            output += '<div class="color_name">';
            output += '<span class="color_block" style="background:' + tarr[5] +'"><span class="top_triangle"></span><span class="bottom_triangle"></span></span>';
            output += '<span class="name">'+ name + '</span>';
            output += '</div>';
            output += '</div>';
        }
    }



    $('.material_list').append(output);
    rebuild_jscroll();
    scrollpane = $('.material_list');
    scrollpane.jScrollPane();
}

function processData_update(allText) {
    var output = "";
    var tarr = allText[0];
    $('.item_group_block_change .button_air').html('<div class="change_to_air" onclick="fill_path_by_id(\'' + tarr[0] +'\');">' + tarr[1] + '</div>').attr('material',tarr[0]);
    for (var i=0; i<allText.length; i++) {
        var name = "";
        tarr = allText[i];

        materials[tarr[0]] = [];
        materials[tarr[0]] = tarr;

        if (tarr[3] != '') {
            name += 'λ = ' + tarr[3];
        }

        if (tarr[4] != '') {
            name  += ', ε = ' + tarr[4];
        }
        if (tarr[2] != '') {
            name  += ', d = ' + tarr[2];
        }
        name += ' <br> ' + tarr[1];
        if (i != 0) {
            output += '<div class="material_item material' + tarr[0] + '" material="' + tarr[0] + '" onclick="fill_path_by_id(\'' + tarr[0] + '\');">';
            output += '<div class="color_name">';
            output += '<span class="color_block" style="background:' + tarr[5] + '"><span class="top_triangle"></span><span class="bottom_triangle"></span></span>';
            output += '<span class="name">' + name + '</span>';
            output += '</div>';
            output += '</div>';
        }
    }
    var api = scrollpane.data('jsp');
    api.getContentPane().html(output);
    api.reinitialise();
}

function show_notification(n,wrap) {
	n  = $(n);
	wrap  = $(wrap);

    n.css('top',(wrap.outerHeight()-n.outerHeight())/2);

    if (n.hasClass('notice_ready') && $(window).width() >= 960) {
        n.css('left',(wrap.outerWidth()-n.outerWidth()+$('.material_list_wrap').outerWidth())/2);
    } else {
        n.css('left',(wrap.outerWidth()-n.outerWidth())/2);
    }
	n.css('display','block');
	wrap.find('.svg_notice_bg').css('display','block');
}

var hexDigits = new Array
("0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f");

//Function to convert hex format to a rgb color
function rgb2hex(rgb) {
    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}

function hex(x) {
    return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
}

function close_notification(n,wrap) {
	$(n).css('display','none');
	$(wrap).find('.svg_notice_bg').css('display','none');
}

function get_x(y,x1,y1,x2,y2) {
    if ( x1 == x2 ) {
        return x1;
    }
    var a = (y2-y1)/(x2-x1);
    var b = (x2*y1-x1*y2)/(x2-x1);
    var x = (y-b)/a;

    return x;
}

var svg_html = '';

function svg_success() {
    $('.svg_wrap svg').on('click',function(){
        svgPanZoom.events.drag= false;
        svgPanZoom.events.drag= true;
    });
    //TODO change ajax normar
    $.get($('.svg_data .url').text(), function (data) {
        var error = 0;
        $('.svg_wrap').append('<div id="svg_temp"></div>');
        $('#svg_temp').append(new XMLSerializer().serializeToString(data.documentElement));
        default_width = $('#svg_temp svg').attr('width');
        default_height = $('#svg_temp svg').attr('height');
        svg_html=$('#svg_temp svg').attr('id','temp_svg');
        var svg = document.getElementById('temp_svg');
        var box = svg.getAttribute('viewBox');
        default_viewbox = box;
        box = box.split(' ');

        var x = box[0];
        var y = box[1];
        var width = box[2];
        var height = box[3];

        draw.viewbox(x, y, width, height);

        svg_html = svg_html[0].childNodes;

        $(svg_html).each(function(){
            $('#svg_temp').html(this);
            draw.svg($('#svg_temp').html());
        });

        $('#svg_temp').remove();

        svgPanZoom = $(".svg_wrap #svg svg").svgPanZoom({
            events: {
                mouseWheel: true,
                doubleClick: false,
                drag: true,
                dragCursor: "move"
            },
            maxZoom: 5,
            panFactor: 0.05,
            /*initialViewBox: { // the initial viewBox, if null or undefined will try to use the viewBox set in the svg tag. Also accepts string in the format "X Y Width Height"
             x: x, // the top-left corner X coordinate
             y: y, // the top-left corner Y coordinate
             width: width, // the width of the viewBox
             height: height, // the height of the viewBox
             },*/
            /*limits: { // the limits in which the image can be moved. If null or undefined will use the initialViewBox plus 15% in each direction
                x: x-width*0.8,
                y: y-height*0.8,
                x2: x+width*1.8,
                y2: y+height*1.8
            }*/
        });

        $(".svg_wrap #svg").swipe( {
            pinchStatus:function(event, phase, direction, distance , duration , fingerCount, pinchZoom) {
                svgPanZoom.events.drag= false;
            },
            pinchIn:function(event, direction, distance, duration, fingerCount, pinchZoom) {
                svgPanZoom.zoomIn(pinchZoom);
                svgPanZoom.events.drag= true;
            },
            pinchOut:function(event, direction, distance, duration, fingerCount, pinchZoom) {
                svgPanZoom.zoomOut(pinchZoom);
                svgPanZoom.events.drag= true;
            },
            fingers:2,
        });

        var callback = function (example) {
            return function (event) {
                if ($(event.target).hasClass("fa-arrow-up"))
                    example.panUp()
                if ($(event.target).hasClass("fa-arrow-down"))
                    example.panDown()
                if ($(event.target).hasClass("fa-arrow-left"))
                    example.panLeft()
                if ($(event.target).hasClass("fa-arrow-right"))
                    example.panRight()
                if ($(event.target).hasClass("fa-plus")) {
                    example.zoomIn();
                }
                if ($(event.target).hasClass("fa-minus")) {
                    example.zoomOut();
                }
                if ($(event.target).hasClass("fa-refresh")) {
                    example.reset();
                }
            }
        };

        $("div#svg_controls i").click(callback(svgPanZoom));

        $('.svg_wrap #svg desc').each(function () {

            var text = $(this).html();

            if (text == 'Lonely Vertices' || text == 'Shady Edges' || text == 'Unused Edges') {
                error = error + 1;
            }
        });
        if (error > 0) {
            show_notification('.notice_error', '.svg_wrap');

            $('.svg_notice .close_notice').click(function () {
                close_notification('.svg_notice', '.svg_wrap');
                return false;
            });
            $('.svg_wrap #svg g').each(function(){
                var stroke_width = parseFloat($(this).css('stroke-width'));
                if (stroke_width < 1) {
                    stroke_width = stroke_width*3;
                    $(this).css('stroke-width',stroke_width);
                }
            });
            $('.svg_wrap #svg circle').each(function(){
                var radius = $(this).attr('r');
                radius = radius*3;
                $(this).attr('r',radius);
            });
        } else {
            $('.svg_wrap #svg path').css('fill','none');
            //TODO change ajax
            openerp.jsonRpc('/page/flixo_get_material_json', 'call', {}).then(function(data) {
                processData(data);
            });
            /*$.ajax({
                type: "GET",
                url: $('.svg_data .csv').text(),
                dataType: "json",
                success: function(data) {
                    processData(data);
                }
            });*/

            //TODO change ajax
            openerp.jsonRpc('/page/flixo_get_product_information_json', 'call', { token:$('.svg_wrap .svg_data .token').text()}).then(function(data) {

            /*$.ajax({
                type: "GET",
                url: $('.svg_data .material_list_group').text(),
                dataType: "json",
                success: function(data) {*/
                    var i = 0;
                    $('.svg_wrap #svg path').each(function () {
                     $(this).attr('id', 'path' + i);
                        i++;
                    });
                    $.each( data, function( key, value ) {
                        if (value['material_id'] != '') {
                            $('.svg_wrap #svg #path' + value['id']).find('matprop').attr('material',value['material_id']);
                            $('.svg_wrap #svg #path' + value['id']).attr('rel',value['id']);
                        }
                    });
                     i = 0;
                     var el_arr = [];
                     var el_arr_mat = [];
                     $('.svg_wrap #svg path').each(function () {
                         $(this).attr('rel', i);
                         if ($(this).find('area').attr('value') > 0.000002) {
                             $(this).attr('id', 'path' + i);
                             var path = SVG.get('path' + i);
                             var tempPoligon = [];
                             $.each(path.array().value, function(index, value) {
                                 if (value[0] == "M") {
                                     if (tempPoligon.length > 0) {
                                         polygonsArr.push(tempPoligon);
                                         tempPoligon = [];
                                     }
                                 }
                                 tempPoligon.push({'x': value[1], 'y': value[2]});
                             });
                             if (tempPoligon.length > 0) {
                                polygonsArr.push(tempPoligon);
                             }


                             if ($(this).find('matprop').attr('material') === undefined || $(this).find('matprop').attr('material') == '{00000000-0000-0000-0000-000000000000}' ) {
                                el_arr_mat.push('#'+$(this).attr('id'));
                             } else {
                                 var material = $(this).find('matprop').attr('material');

                                 if (el_arr[material] === undefined) {
                                     el_arr[material] = [];
                                 }

                                 el_arr[material]['material'] = material;
                                 if (el_arr[material]['ids'] === undefined) {
                                    el_arr[material]['ids'] = [];
                                 }
                                 el_arr[material]['ids'].push('#'+$(this).attr('id'));
                             }
                             if (path.width() > 0.19 || path.height() > 0.19) {
                                window_count = window_count + 1;
                             }
                         } else {
                             var path = SVG.get('path' + i);
                             var tempPoligon = [];
                             $.each(path.array().value, function(index, value) {
                                 if (value[0] == "M") {
                                     if (tempPoligon.length > 0) {
                                         polygonsArr.push(tempPoligon);
                                         tempPoligon = [];
                                     }
                                 }
                                 tempPoligon.push({'x': value[1], 'y': value[2]});
                             });
                             if (tempPoligon.length > 0) {
                                 polygonsArr.push(tempPoligon);
                             }
                         }
                         i = i + 1;
                     });

                     $('.svg_wrap #svg g').attr('id','group_d');
                     $('.svg_wrap #svg g').addClass('group_d');
                     $('.svg_wrap #svg g').append('<g id="temperature"></g>');
                     var group_d = SVG.get('group_d').addTo(draw);

                     $.each( el_arr_mat, function( key, value ) {
                         var temp_arr = [];
                         temp_arr['material'] = 'none';
                         temp_arr['ids'] = [];
                         temp_arr['ids'].push(value);
                         el_arr.push(temp_arr);
                     });

                     for (var key in el_arr) {
                        groups.push(el_arr[key]);
                     }

                     temp_arr = undefined;
                     el_arr = undefined;
                     el_arr_mat = undefined;
                    /* TODO openerp need delete next}*/
                //}
            });


        }

    });
}
/*
function svg_error(){
    $.get($('.svg_data .url_error').text(), function (data) {
        $('.svg_wrap').append('<div id="svg_temp"></div>');
        $('#svg_temp').html(new XMLSerializer().serializeToString(data.documentElement));
        var svg_html=$('#svg_temp svg').attr('id','temp_svg');
        var svg = document.getElementById('temp_svg');
        var box = svg.getAttribute('viewBox');
        box = box.split(' ');

        var x = box[0];
        var y = box[1];
        var width = box[2];
        var height = box[3];

        draw.viewbox(x, y, width, height);

        svg_html = svg_html[0].childNodes;

        $(svg_html).each(function(){
            $('#svg_temp').html(this);
            draw.svg($('#svg_temp').html());
        });

        $('#svg_temp').remove();

        var svgPanZoom; //globals so we can manipulate them in the debugger
        svgPanZoom = $(".svg_wrap #svg svg").svgPanZoom({
            events: {
                mouseWheel: true,
                doubleClick: false,
                drag: true,
                dragCursor: "move"
            }, maxZoom: 5, panFactor: 0.05
        });

        $(".svg_wrap #svg").swipe( {
            pinchStatus:function(event, phase, direction, distance , duration , fingerCount, pinchZoom) {
                svgPanZoom.events.drag= false;
            },
            pinchIn:function(event, direction, distance, duration, fingerCount, pinchZoom) {
                svgPanZoom.zoomIn(pinchZoom);
                svgPanZoom.events.drag= true;
            },
            pinchOut:function(event, direction, distance, duration, fingerCount, pinchZoom) {
                svgPanZoom.zoomOut(pinchZoom);
                svgPanZoom.events.drag= true;
            },
            fingers:2,
        });

        var callback = function (example) {
            return function (event) {
                if ($(event.target).hasClass("fa-arrow-up"))
                    example.panUp()
                if ($(event.target).hasClass("fa-arrow-down"))
                    example.panDown()
                if ($(event.target).hasClass("fa-arrow-left"))
                    example.panLeft()
                if ($(event.target).hasClass("fa-arrow-right"))
                    example.panRight()
                if ($(event.target).hasClass("fa-plus"))
                    example.zoomIn()
                if ($(event.target).hasClass("fa-minus"))
                    example.zoomOut()
                if ($(event.target).hasClass("fa-refresh"))
                    example.reset()
            }
        };

        $("div#svg_controls i").click(callback(svgPanZoom));

        show_notification('.notice_error', '.svg_wrap');

        $('.svg_notice .close_notice').click(function () {
            close_notification('.svg_notice', '.svg_wrap');
            return false;
        });
    });
}
*/
function line_width(x1,x2,y1,y2) {
    return Math.sqrt(Math.pow((x2-x1),2)+Math.pow((y2-y1),2));
}

function choose_temperature() {

    $('#chose_temperature_point span').each(function(key){
        temperature.push({'class' : $(this).attr('class'), 'url' : $(this).css('background-image').slice(4, -1).replace(/"/g, ""), 'position' : '' })
        $(this).attr('id','temperature' + key).attr('rel',key);
    });

    $('#chose_temperature_point span').click(function(){

        current_temperature = $(this).attr('rel');
        if (!$('#chose_temperature_point #temperature'  + current_temperature).hasClass('active')) {
            $('#chose_temperature_point, #svg').attr('class','');
            $('#chose_temperature_point, #svg').addClass(temperature[current_temperature]['class']);
            $('#chose_temperature_point').addClass('moved');
            $('#chose_temperature_point span').removeClass('active');
            $('#chose_temperature_point #temperature'  + current_temperature).addClass('active');
        } else {
            $('#chose_temperature_point, #svg').attr('class','');
            $('#chose_temperature_point, #svg').removeClass(temperature[current_temperature]['class']);
            $('#chose_temperature_point').removeClass('moved');
            $('#chose_temperature_point span').removeClass('active');
        }
    });

    $("#svg svg").on('click', 'image', function(evt) {
        var img_id= $(this).attr('id');
        $('#chose_temperature_point #temperature'  + $('svg [rel_img="' + img_id +'"]').attr('rel_id')).removeClass('hidden').removeClass('active');
        var id_bcprop = $('svg [rel_img="' + img_id +'"]').attr('id');
        var i=0;

        $(this).remove();
        $('svg [rel_img="' + img_id +'"]').remove();
        if (id_bcprop == 'External') {
            $('svg bcprop').each(function(){
                if($(this).attr('id') == 'External') {
                    i = i + 1;
                }
            });
            if (i == 0) {
                blue_temperature = "";
            } else {
                blue_temperature = $('svg [id="External"]').attr('rel');
            }
        }
        if (id_bcprop == 'Interior') {
            $('svg bcprop').each(function(){
                if($(this).attr('id') == 'Interior') {
                    i = i + 1;
                }
            });
            if (i == 0) {
                red_temperature = "";
            } else {
                red_temperature = $('svg [id="Interior"]').attr('rel');
            }
        }
    });
    $("#svg").on('click', 'svg', function(evt) {
        if (!$('#chose_temperature_point').hasClass('moved')) {
            return false;
        }

        var svg = document.querySelector('svg');
        var pt = svg.createSVGPoint();
        pt.x = evt.clientX;
        pt.y = evt.clientY;

        var loc =  pt.matrixTransform(svg.getScreenCTM().inverse());

        var my_polinome = polygonsArr[0];
        var array_points = [];
        npol = polygonsArr[0].length;
        j = 0;

        for (k = 0; k < polygonsArr.length; k++){
            npol = polygonsArr[k].length;
            var my_polinome = polygonsArr[k];
            var j = 0;
            for (i = 1; i < npol; i++){
                if ((loc.y >= my_polinome[j].y && loc.y <= my_polinome[i].y) || (loc.y <= my_polinome[j].y && loc.y >= my_polinome[i].y)) {
                    var points = [];
                    points['x'] = my_polinome[j].x;
                    points['y'] = my_polinome[j].y;
                    points['x2'] = my_polinome[i].x;
                    points['y2'] = my_polinome[i].y;
                    array_points.push(points);
                }
                j = i;
            }
            if ((loc.y >= my_polinome[0].y && loc.y <= my_polinome[npol-1].y) || (loc.y <= my_polinome[0].y && loc.y >= my_polinome[npol-1].y)) {
                var points = [];
                points['x'] = my_polinome[0].x;
                points['y'] = my_polinome[0].y;
                points['x2'] = my_polinome[npol-1].x;
                points['y2'] = my_polinome[npol-1].y;
                array_points.push(points);
            }
        }

        var min_x = '';
        var max_x = '';
        var min_points = [];
        var max_points = [];
        for (i = 0; i < array_points.length; i++){
            var temp_x = get_x(loc.y,array_points[i]['x'],array_points[i]['y'],array_points[i]['x2'],array_points[i]['y2']);

            if (min_x == '') {
                min_x = temp_x;
                max_x = temp_x;
                min_points = array_points[i];
                max_points = array_points[i];
            }
            if (min_x > temp_x) {
                min_x = temp_x;
                min_points = array_points[i];
            }
            if (max_x < temp_x) {
                max_x = temp_x;
                max_points = array_points[i];
            }
        }
        var imageW = 0.005;
        var imageH = 0.005;
        var group_d = SVG.get('group_d');
        var current_x = '';
        var current_y = '';

        if (array_points.length > 0) {
            /****************Вычисление точек*********************/

            if ($('#chose_temperature_point #temperature'+current_temperature).hasClass('blue_point')) {
                if (blue_temperature == ''){
                    if  (line_width(min_x,loc.x,loc.y,loc.y) < line_width(max_x,loc.x,loc.y,loc.y)) {
                        blue_temperature="min";
                        if  (line_width(min_x,min_points['x'],loc.y,min_points['y']) < line_width(min_x,min_points['x2'],loc.y,min_points['y2'])) {
                            current_x = min_points['x'];
                            current_y = min_points['y'];
                        } else {
                            current_x = min_points['x2'];
                            current_y = min_points['y2'];
                        }
                    } else {
                        blue_temperature="max";
                        if  (line_width(max_x,max_points['x'],loc.y,max_points['y']) < line_width(max_x,max_points['x2'],loc.y,max_points['y2'])) {
                            current_x = max_points['x'];
                            current_y = max_points['y'];
                        } else {
                            current_x = max_points['x2'];
                            current_y = max_points['y2'];
                        }
                    }
                } else if (blue_temperature == 'min') {
                    blue_temperature="max";
                    if  (line_width(max_x,max_points['x'],loc.y,max_points['y']) < line_width(max_x,max_points['x2'],loc.y,max_points['y2'])) {
                        current_x = max_points['x'];
                        current_y = max_points['y'];
                    } else {
                        current_x = max_points['x2'];
                        current_y = max_points['y2'];
                    }
                } else {
                    blue_temperature="min";
                    if  (line_width(min_x,min_points['x'],loc.y,min_points['y']) < line_width(min_x,min_points['x2'],loc.y,min_points['y2'])) {
                        current_x = min_points['x'];
                        current_y = min_points['y'];
                    } else {
                        current_x = min_points['x2'];
                        current_y = min_points['y2'];
                    }
                }
            }
            if ($('#chose_temperature_point #temperature'+current_temperature).hasClass('red_point')) {
                if (red_temperature == ''){
                    if  (line_width(min_x,loc.x,loc.y,loc.y) < line_width(max_x,loc.x,loc.y,loc.y)) {
                        red_temperature="min";
                        if  (line_width(min_x,min_points['x'],loc.y,min_points['y']) < line_width(min_x,min_points['x2'],loc.y,min_points['y2'])) {
                            current_x = min_points['x'];
                            current_y = min_points['y'];
                        } else {
                            current_x = min_points['x2'];
                            current_y = min_points['y2'];
                        }
                    } else {
                        red_temperature="max";
                        if  (line_width(max_x,max_points['x'],loc.y,max_points['y']) < line_width(max_x,max_points['x2'],loc.y,max_points['y2'])) {
                            current_x = max_points['x'];
                            current_y = max_points['y'];
                        } else {
                            current_x = max_points['x2'];
                            current_y = max_points['y2'];
                        }
                    }
                } else if (red_temperature == 'min') {
                    red_temperature="max";
                    if  (line_width(max_x,max_points['x'],loc.y,max_points['y']) < line_width(max_x,max_points['x2'],loc.y,max_points['y2'])) {
                        current_x = max_points['x'];
                        current_y = max_points['y'];
                    } else {
                        current_x = max_points['x2'];
                        current_y = max_points['y2'];
                    }
                } else {
                    red_temperature="min";
                    if  (line_width(min_x,min_points['x'],loc.y,min_points['y']) < line_width(min_x,min_points['x2'],loc.y,min_points['y2'])) {
                        current_x = min_points['x'];
                        current_y = min_points['y'];
                    } else {
                        current_x = min_points['x2'];
                        current_y = min_points['y2'];
                    }
                }
            }

            var image = draw.image(temperature[current_temperature]['url'], imageW, imageH);

            image.move(current_x - imageW / 2, current_y - imageH / 2);

            image.front().attr('class', 'image_temperature');
            $('#temperature' + current_temperature).addClass('hidden');
            if (temperature[current_temperature]['class'] == 'blue_point') {
                $('#svg svg #temperature').append("<bcProp id='External' x='" + current_x + "' y='" + current_y + "' temp='273.15' rs='0.04' rel_img='" + image.attr('id') + "' rel_id='" +current_temperature+ "' rel='" + blue_temperature + "'/>");
            } else{
                $('#svg svg #temperature').append("<bcProp id='Interior' x='" + current_x + "' y='" + current_y + "' temp='293.15' rs='0.13' rel_img='" + image.attr('id') + "' rel_id='" +current_temperature+ "' rel='" + red_temperature + "'/>");
            }
            if($('#chose_temperature_point span').not('#chose_temperature_point span.hidden').length == 0) {
                $('.chose_temperature .temperature_ready').removeClass('hidden');
            }
            svgPanZoom.setCenter(current_x, loc.y);
            $('#chose_temperature_point, #svg').attr('class','');
            $('.chose_temperature .button_reset_temperature').click(function(){
                $('#svg svg .image_temperature,#svg svg bcProp').remove();
                $('#chose_temperature_point span').removeClass('hidden');
                $('#chose_temperature_point span').removeClass('active');
                blue_temperature = '';
                red_temperature = '';
            });
        }

    });
}

$(document).ready(function(){
    if (!SVG.supported) {
        alert('SVG not supported')
    } else if ($('.svg_wrap').length >= 1){

        draw = SVG('svg').size('100%', '100%');

        if ($('.svg_wrap .url').length > 0) {
            svg_success();
            show_notification('.notice_congratulation', '.svg_wrap');

            $('.svg_notice .close_notice').on( "click",function () {
                close_notification('.svg_notice', '.svg_wrap');
                if ($(this).closest('.svg_notice').hasClass('notice_congratulation')) {
                    $('.svg_wrap .item_group_block').css('display','block');
                    next_fill();
                }
                return false;
            });
            $('.button_yes, .button_next').click(function(){
                next_element();
            });
            $('.button_prev').click(function(){
                prev_element();
            });
            $('.button_no').click(function(){
                $('.item_group_block').css('display', 'none');
                $('.item_group_block_change').css('display', 'block');
                rebuild_jscroll();
                var api = scrollpane.data('jsp');
                api.reinitialise();
                svg_center();
            });
            $('.button_menu').click(function(){
                $('.item_group_block').css('display', 'block');
                $('.item_group_block_change').css('display', 'none');
                svg_center();
            });

            $('.material_list_top input').on('keyup',function(){
                if($(this).val().length >= 3) {
                    $('.material_item').css('display','block');
                    $('.material_item .name').not(".material_item .name:icontains('" + $(this).val() +"')").closest('.material_item').css('display','none');
                    var api = scrollpane.data('jsp');
                    api.reinitialise();
                } else {
                    $('.material_item').css('display','block');
                    var api = scrollpane.data('jsp');
                    api.reinitialise();
                }
            });
            $('.svg_wrap .button_ungroup').click(function(){
                ungroup_groups();
            });
            $('.svg_wrap .button_ready').click(function(){
                show_notification('.notice_ready', '.svg_wrap');
                ready_buttons(true);
                list_material_array = [];

                var jsonArg2 = new Object();
                $('.svg_wrap #svg svg path').each(function(){
                    var jsonArg2 = new Object();
                    jsonArg2.id = jQuery(this).attr('rel');
                    jsonArg2.material = jQuery(this).find('matprop').attr('material');
                    list_material_array.push(jsonArg2);
                });
            });
            $('.svg_wrap .button_create').click(function(){
                $('.item_group_block_change').css('display', 'none');
                $('.create_material').css('display', 'block');
                var newColor = '#'+(0x1000000+(Math.random())*0xffffff).toString(16).substr(1,6);
                $(".create_material .color").css('background',newColor);
            });
            $('.svg_wrap .close_create').click(function(){
                $('.item_group_block_change').css('display', 'block');
                $('.create_material').css('display', 'none');
            });
            $('.svg_wrap .create_ready').click(function(){
                var error = false;
                $('.svg_wrap .inputs input,.svg_wrap .inputs textarea').removeClass('error');

                if ($('.svg_wrap .lambda').val() == '') {
                    error = true;
                    $('.svg_wrap .lambda').addClass('error');
                }

                if ($('.svg_wrap .name').val() == '') {
                    error = true;
                    $('.svg_wrap .name').addClass('error');
                }
                if (error == true) {
                    return false;
                }
                //TODO change ajax

                 openerp.jsonRpc('/page/flixo_create_material_json', 'call', {
                 name:$('.svg_wrap .create_material .name').val(),
                 color:rgb2hex($('.svg_wrap .create_material .color').css('background-color')),
                 lambda:$('.svg_wrap .create_material .lambda').val(),
                 eps:$('.svg_wrap .create_material .eps').val(),
                 dencity:$('.svg_wrap .create_material .dencity').val(),
                 }).then(function(data){

               /* $.post("/ajax/create_material.php",
                    {
                        name:$('.svg_wrap .create_material .name').html(),
                        color:rgb2hex($('.svg_wrap .create_material .color').css('background-color')),
                        lambda:$('.svg_wrap .create_material .lambda').val(),
                        eps:$('.svg_wrap .create_material .eps').val(),
                        density:$('.svg_wrap .create_material .dencity').val(),
                    },
                function(data){*/
                    var new_material = data;
                    var output=append_created($('.svg_wrap .create_material .name').val(),$('.svg_wrap .create_material .color').css('background-color'),$('.svg_wrap .create_material .lambda').val(),$('.svg_wrap .create_material .eps').val(),$('.svg_wrap .create_material .dencity').val(),new_material);
                    var api = scrollpane.data('jsp');
                    api.getContentPane().prepend(output);
                    fill_path_by_id(new_material);
                    $('.item_group_block_change').css('display', 'block');
                    $('.create_material').css('display', 'none');
                    rebuild_jscroll();
                    api.reinitialise();
                    api.scrollTo(0, parseInt($(".material"+new_material).offset().top-$(".material_list").offset().top));
                },"json");
            });
            $('.svg_wrap .temperature_ready').click(function(){
                show_notification('.notice_temperature', '.svg_wrap');
            });
            $('.svg_wrap .button_cheak').click(function(){
                close_notification('.notice_temperature', '.svg_wrap');
            });
            $('.svg_wrap .chose_temperature .button_interior,#cabinet-container.flixo.svg .flixo_step2').click(function(){
                $('.svg_wrap .chose_temperature').css('display','none');
                $('.item_group_block').css('display', 'block');
                $('.flixo_step2').removeClass('selectd_prev').addClass('active');
                $('.flixo_step3').removeClass('active');
                $('#chose_temperature_point, #svg').attr('class','');
                $('.svg_wrap svg image').css('display','none');
                //current_group = groups.length - 1 ;
                var material_id = groups[current_group]['material'];
                fill_path(current_group, material_id);
                ready_buttons(false);
                return false;
            });
            $('.svg_wrap .notice_ready .ready_no').click(function(){
                close_notification('.notice_ready', '.svg_wrap');
                return false;
            });
            $('.svg_wrap .notice_ready .ready_button').click(function(){
                close_notification('.notice_ready', '.svg_wrap');
                $('.svg_wrap .chose_temperature').css('display','block');
                $('.item_group_block').css('display', 'none');
                $('.item_group_block_change').css('display', 'none');
                $('.flixo_step2').addClass('selectd_prev').removeClass('active');
                $('.flixo_step3').addClass('active');
                $('.svg_wrap svg image').css('display','block');
                $('.svg_wrap #svg svg path').css('fill-opacity',"1");
                return false;
            });
            choose_temperature();
            $('.svg_wrap .button_result').click(function(){
                var mass_push=[];
                $('.svg_wrap path').removeAttr('rel');
                $('.svg_wrap path').removeAttr('id');
                $('.svg_wrap path matprop').removeAttr('material');
                $('.svg_wrap path bcprop').removeAttr('rel_id');
                $('.svg_wrap path bcprop').removeAttr('rel');
                $('.svg_wrap path bcprop').removeAttr('rel_img');
                $('.svg_wrap .image_temperature').remove();
                if (window_count > 1) {
                    mass_push['frame'] = 'internal';
                } else {
                    mass_push['frame'] = 'external';
                }
                $('.svg_wrap #svg svg').attr('width',default_width);
                $('.svg_wrap #svg svg').attr('height',default_height);
                $('.svg_wrap #svg svg').removeAttr('xmlns');
                $('.svg_wrap #svg svg').removeAttr('xmlns:xlink');
                $('.svg_wrap #svg svg').removeAttr('xmlns:svgjs');
                $('.svg_wrap #svg svg').removeAttr('id');
                $('.svg_wrap #svg svg').removeAttr('preserveAspectRatio');
                $('.svg_wrap #svg svg').removeAttr('preserveaspectratio');
                var box = default_viewbox;
                box = box.split(' ');

                var x = box[0];
                var y = box[1];
                var width = box[2];
                var height = box[3];

                draw.viewbox(x, y, width, height);
                mass_push['svg']='<?xml version="1.0" encoding="UTF-8"?>' + $('.svg_wrap #svg').html();
                mass_push['material_list']=JSON.stringify(list_material_array);
                console.log(mass_push['svg']);
                $('.preload_data').css('display','block');
                //TODO change ajax

                 openerp.jsonRpc('/page/post_svg_file_json', 'call', {
                 frame:mass_push['frame'],
                 svg:mass_push['svg'],
                 material_list:mass_push['material_list'],
                 token:$('.svg_wrap .svg_data .token').text(),
                 }).then(function(data){


                   /* $ .post("/ajax/create_material.php",
                    {
                        frame:mass_push['frame'],
                        svg:mass_push['svg'],
                        material_list:mass_push['material_list'],
                        token:$('.svg_wrap .svg_data .token').text(),
                    },
                    function(data){*/
                        /*var intervalID;
                        intervalID  = setInterval(function() {
                            openerp.jsonRpc('/ajax/go_calculate.php', 'call', {token:$('.svg_wrap .svg_data .token').text()}).then(function(data){*/
                            /*$.ajax({
                                type: "GET",
                                url: "/ajax/go_calculate.php",
                                dataType: "json",
                                success: function(data) {*/
                                    if (data['error'] == 'True') {
                                        $('.notice_error_post .text').html(data['error_message']);
                                        show_notification('.notice_error_post', '.svg_wrap');
                                        //clearInterval(intervalID);
                                    }
                                    if (data['error'] == 'False') {
                                        if (data['link'] != '') {
                                            window.location = data['link'];
                                           // clearInterval(intervalID);
                                        }
                                    }
                                //}
                            //});
                           /* },"json");
                        }, 5000);*/
                    },"json");

            });
        }
    } else if ($('.svg_wrap_result').length >= 1) {

        draw_material = SVG('show_material').size('100%', '100%');
        //draw_isoterm = SVG('show_isoterm').size('100%', '100%');
        //TODO change ajax normal
        $.get($('.show_material .svg_data .url').text(), function (data) {
            $('.svg_wrap_result .show_material').append('<div id="svg_temp"></div>');
            $('#svg_temp').html(new XMLSerializer().serializeToString(data.documentElement));
            $('#svg_temp svg').attr('id','temp_svg');
            var svg = document.getElementById('temp_svg');
            var box = svg.getAttribute('viewBox');
            box = box.split(' ');

            var x = box[0];
            var y = box[1];
            var width = box[2];
            var height = box[3];

            draw_material.viewbox(x, y, width, height);

            draw_material.svg($('#svg_temp svg').html());
            $('#svg_temp').remove();

            svgPanZoom = $(".svg_wrap_result .show_material svg").svgPanZoom({
                events: {
                    mouseWheel: true,
                    doubleClick: false,
                    drag: true,
                    dragCursor: "move"
                }, maxZoom: 5, panFactor: 0.05
            });
            $(".svg_wrap_result .show_material .svg.show_material").swipe( {
                pinchStatus:function(event, phase, direction, distance , duration , fingerCount, pinchZoom) {
                    svgPanZoom.events.drag= false;
                },
                pinchIn:function(event, direction, distance, duration, fingerCount, pinchZoom) {
                    svgPanZoom.zoomIn(pinchZoom);
                    svgPanZoom.events.drag= true;
                },
                pinchOut:function(event, direction, distance, duration, fingerCount, pinchZoom) {
                    svgPanZoom.zoomOut(pinchZoom);
                    svgPanZoom.events.drag= true;
                },
                fingers:2,
            });

            var callback = function (example) {
                return function (event) {
                    if ($(event.target).hasClass("fa-arrow-up"))
                        example.panUp()
                    if ($(event.target).hasClass("fa-arrow-down"))
                        example.panDown()
                    if ($(event.target).hasClass("fa-arrow-left"))
                        example.panLeft()
                    if ($(event.target).hasClass("fa-arrow-right"))
                        example.panRight()
                    if ($(event.target).hasClass("fa-plus"))
                        example.zoomIn()
                    if ($(event.target).hasClass("fa-minus"))
                        example.zoomOut()
                    if ($(event.target).hasClass("fa-refresh"))
                        example.reset()
                }
            };

            $("div.show_material_control.svg_controls i").click(callback(svgPanZoom));

        });
        //TODO change ajax normal
        /*
        $.get($('.show_isoterm .svg_data .url').text(), function (data) {
            $('.svg_wrap_result .show_isoterm').append('<div id="svg_temp2"></div>');
            $('#svg_temp2').html(new XMLSerializer().serializeToString(data.documentElement));
            $('#svg_temp2 svg').attr('id','temp_svg');
            var svg = document.getElementById('temp_svg');
            var box = svg.getAttribute('viewBox');
            box = box.split(' ');

            var x = box[0];
            var y = box[1];
            var width = box[2];
            var height = box[3];

            draw_isoterm.viewbox(x, y, width, height);

            draw_isoterm.svg($('#svg_temp2 svg').html());
            $('#svg_temp2').remove();

            svgPanZoom = $(".svg_wrap_result .show_isoterm svg").svgPanZoom({
                events: {
                    mouseWheel: true,
                    doubleClick: false,
                    drag: true,
                    dragCursor: "move"
                }, maxZoom: 5, panFactor: 0.05
            });
            $(".svg_wrap_result .show_isoterm .svg.show_isoterm").swipe( {
                pinchStatus:function(event, phase, direction, distance , duration , fingerCount, pinchZoom) {
                    svgPanZoom.events.drag= false;
                },
                pinchIn:function(event, direction, distance, duration, fingerCount, pinchZoom) {
                    svgPanZoom.zoomIn(pinchZoom);
                    svgPanZoom.events.drag= true;
                },
                pinchOut:function(event, direction, distance, duration, fingerCount, pinchZoom) {
                    svgPanZoom.zoomOut(pinchZoom);
                    svgPanZoom.events.drag= true;
                },
                fingers:2,
            });

            var callback = function (example) {
                return function (event) {
                    if ($(event.target).hasClass("fa-arrow-up"))
                        example.panUp()
                    if ($(event.target).hasClass("fa-arrow-down"))
                        example.panDown()
                    if ($(event.target).hasClass("fa-arrow-left"))
                        example.panLeft()
                    if ($(event.target).hasClass("fa-arrow-right"))
                        example.panRight()
                    if ($(event.target).hasClass("fa-plus"))
                        example.zoomIn()
                    if ($(event.target).hasClass("fa-minus"))
                        example.zoomOut()
                    if ($(event.target).hasClass("fa-refresh"))
                        example.reset()
                }
            };

            $("div.show_isoterm_control.svg_controls i").click(callback(svgPanZoom));

        });
*/
        /*$('.show_isoterms').click(function(){
            $(this).css('display','none');
            $('.show_materials').css('display','inline-block');
            $('.show_material,.show_isoterm,.show_calculate').css('display' , 'none');
            $('.show_isoterm').css('display' , 'block');
            return false;
        });*/
        $('.show_materials').click(function(){
            $(this).css('display','none');
            $('.result_calculate').css('display','inline-block');
            $('.show_material,.show_isoterm,.show_calculate').css('display' , 'none');
            $('.show_material').css('display' , 'block');
            return false;
        });
        $('.result_calculate').click(function(){
            $('.show_material,.show_isoterm,.show_calculate').css('display' , 'none');
            $('.show_calculate').css('display' , 'block');
            $('.show_materials').css('display','inline-block');
            $('.result_calculate').css('display','none');
            return false;
        });
    }
});

$(window).resize(function(){
    rebuild_jscroll();
});

function fill_path_by_id(material_id) {
    if ($('.svg_wrap .material_item.material'+material_id).hasClass('active')) {
        next_element();
        console.log('123');
    } else {
        fill_path(current_group, material_id);
        groups[current_group]['material']=material_id;
    }
}

function ungroup_groups() {

    var ids = groups[current_group]['ids'];
    groups[current_group]['ids']=[];
    groups[current_group]['ids'].push(ids[0]);

    $('.svg_wrap #svg svg path').css('fill-opacity',".3");
    $('.svg_wrap #svg svg').find(ids[0]).css('fill-opacity', '1');
    jQuery('.svg_wrap .button_ungroup').addClass('hidden');

    var temp_arr = [];

    for (var j = 1; j < ids.length; j++) {
        temp_arr = [];
        temp_arr['material'] = groups[current_group]['material'];
        temp_arr['ids'] = [];
        temp_arr['ids'].push(ids[j]);

        groups.splice(current_group + j,0,temp_arr);
    }
}

function svg_center(){
    var center_x = 0;
    var center_y = 0;
    var center_x2 = 0;
    var center_y2 = 0;

    for ( var i = 0, l = groups[current_group]['ids'].length; i < l; i++) {
        var element = SVG.get(groups[current_group]['ids'][i]);
        var x = element.x();
        var y = element.y();
        var x2 = element.x() + element.width();
        var y2 = element.y() + element.height();
        if (center_x == 0) {
            center_x = x;
        } else if (center_x > x){
            center_x = x;
        }
        if (center_y == 0) {
            center_y = y;
        } else if (center_y > y){
            center_y = y;
        }
        if (center_x2 == 0) {
            center_x2 = x2;
        } else if (center_x2 < x2){
            center_x2 = x2;
        }
        if (center_y2 == 0) {
            center_y2 = y2;
        } else if (center_y2 < y2){
            center_y2 = y2;
        }
    }
    var my_x = (center_x2+center_x)/2;
    var my_y = (center_y2+center_y)/2;

    if ($(window).width() >= 960) {
        if ($('.svg_wrap .item_group_block_change').css('display') == 'block') {
            my_x = my_x*1.2;
        }
    }

    svgPanZoom.setCenter(my_x, my_y);
}

function fill_path(cg, material_id) {
    var color = materials[material_id][5];
    var lambda = materials[material_id][3];
    var eps = materials[material_id][4];
    var dencity = materials[material_id][2];
    var name_change = materials[material_id][1];
    var type = materials[material_id][8];
    var paths = groups[cg]['ids'].join(',');

    $( ".svg_wrap .material_item" ).removeClass( "active" );

    $('.svg_wrap #svg svg path').css('fill-opacity',".3");
    $('.svg_wrap .item_group_block .color_block').css('background-color',color);
    $('.svg_wrap #svg svg').find(paths).css('fill', color);
    $('.svg_wrap #svg svg').find(paths).css('fill-opacity', '1');
    $('.svg_wrap #svg svg').find(paths).find('matprop').attr('material', material_id);
    $('.svg_wrap #svg svg').find(paths).find('matprop').attr('type', type);
    $('.svg_wrap #svg svg').find(paths).find('matprop').attr('id', material_id);
    if (lambda != '') {
        $('.svg_wrap #svg svg').find(paths).find('matprop').attr('lambda', lambda);
        name_change  += ' λ = ' + lambda;
    } else {
        $('.svg_wrap #svg svg').find(paths).find('matprop').removeAttr('lambda');
    }
    if (eps != '') {
        $('.svg_wrap #svg svg').find(paths).find('matprop').attr('eps', eps);
        name_change  += ' ε = ' + eps;
    } else {
        $('.svg_wrap #svg svg').find(paths).find('matprop').removeAttr('eps');
    }
    if (dencity != '') {
        $('.svg_wrap #svg svg').find(paths).find('matprop').attr('density', dencity);
        name_change  += ' d = ' + dencity;
    } else {
        $('.svg_wrap #svg svg').find(paths).find('matprop').removeAttr('density');
    }
    $('.svg_wrap .item_group_block .name_change').html(name_change);
    $( ".svg_wrap .material_item[material='" + material_id + "']" ).addClass( "active" );
}

function next_fill_fun(material_id) {
    svgPanZoom.reset();
    if(groups[current_group]['ids'].length > 1) {
        jQuery('.svg_wrap .button_ungroup').removeClass('hidden');
    } else {
        jQuery('.svg_wrap .button_ungroup').addClass('hidden');
        var box = draw.viewbox()
        var zoom = box.zoom;
        console.log(zoom);
        var area = $('.svg_wrap svg ' + groups[current_group]['ids'][0]).find('area').attr('value');
        if (area < 0.0001) {
            console.log(area);
            svgPanZoom.zoomIn(2);
        }
    }

    $( ".svg_wrap .material_item" ).removeClass( "active" );
    svg_center();
    if (material_id != 'none') {
        $('.item_group_block_change .button_menu').css('display','inline-block');
        fill_path(current_group, material_id);
    } else {
        //fill_path_by_id($('.material_list_wrap .button_air').attr('material'));
        $('.item_group_block_change .button_menu').css('display','none');
        $('.item_group_block').css('display', 'none');
        $('.item_group_block_change').css('display', 'block');
        rebuild_jscroll();
        var api = scrollpane.data('jsp');
        api.reinitialise();
        //side_move = 'empty_right';
        $('.svg_wrap #svg svg path').css('fill-opacity',".3");
        var paths = groups[current_group]['ids'].join(',');
        $('.svg_wrap #svg svg').find(paths).css('fill-opacity', '1');
        $('.svg_wrap #svg svg').find(paths).css('fill', '#ffffff');
    }
}

function next_fill() {
    var material_id = groups[current_group]['material'];

    if (list_material != material_id) {
        list_material = material_id;
        //TODO change ajax

         openerp.jsonRpc('/page/flixo_get_material_json', 'call', {
         material_id:material_id,
         }).then(function(data) {
         processData_update(data);
         next_fill_fun(material_id);
         $('.svg_wrap .material_list_top input').val('');
         $('.svg_wrap .material_item').css('display','block');
         });


        /*$.ajax({
            type: "GET",
            url: $('.svg_data .csv_second').text(),
            dataType: "json",
            success: function(data) {
                processData_update(data);
                next_fill_fun(material_id);
                $('.svg_wrap .material_list_top input').val('');
                $('.svg_wrap .material_item').css('display','block');
            }
        });*/
    } else {
        next_fill_fun(material_id);
    }

}

function next_element() {
    var material_id = groups[current_group]['material'];
    if (material_id != 'none') {
        current_group = current_group +1;
        if (current_group == groups.length) {
            ready_buttons(true);
            current_group = current_group - 1;
        } else {
            next_fill();
        }
    }
}

function ready_buttons(ready) {
    if (ready == true) {
        jQuery('.button_ready').addClass('visible');
        jQuery('.button_yes, .button_next').addClass('hidden');
    } else {
        jQuery('.button_ready').removeClass('visible');
        jQuery('.button_yes, .button_next').removeClass('hidden');
    }
}

function prev_element_fun(material_id) {
    if(groups[current_group]['ids'].length > 1) {
        jQuery('.svg_wrap .button_ungroup').removeClass('hidden');
    } else {
        jQuery('.svg_wrap .button_ungroup').addClass('hidden');
    }
    $('.item_group_block_change .button_menu').css('display','inline-block');
    svg_center();
    fill_path(current_group, material_id);
    var paths = groups[current_group]['ids'].join(',');
    $('.svg_wrap #svg svg path').css('fill-opacity',".3");
    $('.svg_wrap #svg svg').find(paths).css('fill-opacity', '1');
    ready_buttons(false);
}
function prev_element() {
    if (current_group == 0) {
        return false;
    }

    current_group = current_group - 1;
    var material_id = groups[current_group]['material'];

    if (list_material != material_id) {
        list_material = material_id;
        //TODO change ajax

          openerp.jsonRpc('/page/flixo_get_material_json', 'call', {
            material_id:material_id,
            }).then(function(data) {
                    processData_update(data);
                    prev_element_fun(material_id);
                     $('.svg_wrap .material_list_top input').val('');
                     $('.svg_wrap .material_item').css('display','block');

             });

        /*$.ajax({
            type: "GET",
            url: $('.svg_data .csv_second').text(),
            dataType: "json",
            success: function(data) {
                processData_update(data);
                prev_element_fun(material_id);
                $('.svg_wrap .material_list_top input').val('');
                $('.svg_wrap .material_item').css('display','block');
            }
        });*/
    } else {
        prev_element_fun(material_id);
    }
   /*
    if(groups[current_group]['ids'].length > 1) {
        jQuery('.svg_wrap .button_ungroup').removeClass('hidden');
    } else {
        jQuery('.svg_wrap .button_ungroup').addClass('hidden');
    }
    svg_center();
    fill_path(current_group, material_id);
    var paths = groups[current_group]['ids'].join(',');
    $('.svg_wrap #svg svg path').css('fill-opacity',".3");
    $('.svg_wrap #svg svg').find(paths).css('fill-opacity', '1');
    ready_buttons(false);*/
}
