<?php
  $data = array(
    'Uf' => 0.7,
    'bf' => 0.05,
    'q' => 13.4451014615107,
    'up1' => 1.1686143572621,
    'bp1' => 0.2,
    'dT' => 20,
    'bf' => 0.181,
    'frame' => 'external', //internal or external
    'Wg' => array(
      array(
        'title' => 'Aluminium (EN ISO 10077-1)',
        'value' => 0.080
      )
    ),
    'Ws_window' => array(
      array (
        'title' => 'Fixed addition (Uw, BW = Uw + 0,4)',
        'value' => 0.4,
        'type' => 'fixed' //fixed or material
      ),
      array (
        'title' => 'Aluminium (EN ISO 10077-1)',
        'value' => 0.080,
        'type' => 'material' //fixed or material
      )
    ),
    'Ws_internal' => array(
      array (
        'title' => 'Muntin bars (21,4x9,4)',
        'value' => 0.020,
        'type' => 'material' //fixed or material
      ),
      array (
        'title' => 'Muntin bars (25,4x9,4)',
        'value' => 0.022,
        'type' => 'material' //fixed or material
      ),
      array (
        'title' => 'Muntin bars (31,4x9,4)',
        'value' => 0.026,
        'type' => 'material' //fixed or material
      ),
      array (
        'title' => 'Muntin bars (21,4x11,4)',
        'value' => 0.021,
        'type' => 'material' //fixed or material
      ),
      array (
        'title' => 'Muntin bars (25,4x11,4)',
        'value' => 0.024,
        'type' => 'material' //fixed or material
      ),
      array (
        'title' => 'Muntin bars (31,4x11,4)',
        'value' => 0.028,
        'type' => 'material' //fixed or material
      ),
      array (
        'title' => 'Fixed addition (EN 14351-1) profile muntin simple cross (Uw, BW = Uw + 0,1)',
        'value' => 0.1,
        'type' => 'fixed' //fixed or material
      ),
      array (
        'title' => 'Fixed addition (EN 14351-1) profile muntin multiple cross (Uw, BW = Uw + 0,2)',
        'value' => 0.2,
        'type' => 'fixed' //fixed or material
      ),
    ),
    'glass_info' => array(
      'double' => array(
        'material' => array(
          'double_glass1' => 'Uncoated glass (normal glass) 0,89',
          'double_glass2' => 'One pane coated glass ≤0,4',
          'double_glass3' => 'One pane coated glass ≤0,2',
          'double_glass4' => 'One pane coated glass ≤0,1',
          'double_glass5' => 'One pane coated glass ≤0,05'
        ),
        'dimensions' => array(
          '4-6-4' => '4-6-4',
          '4-9-4' => '4-9-4',
          '4-12-4' => '4-12-4',
          '4-15-4' => '4-15-4',
          '4-20-4' => '4-20-4'
        ),
        'gas_type' => array(
          'air' => 'Air',
          'argon' => 'Argon',
          'krypton' => 'Krypton',
          'sf6' => 'SF6'
        )
      ),
      'triple' => array(
        'material' => array(
          'triple_glass1' => 'Uncoated glass (normal glass) 0,89',
          'triple_glass2' => 'One pane coated glass ≤0,4',
          'triple_glass3' => 'One pane coated glass ≤0,2',
          'triple_glass4' => 'One pane coated glass ≤0,1',
          'triple_glass5' => 'One pane coated glass ≤0,05'
        ),
        'dimensions' => array(
          '4-6-4-6-4' => '4-6-4-6-4',
          '4-9-4-9-4' => '4-9-4-9-4',
          '4-12-4-12-4' => '4-12-4-12-4'
        ),
        'gas_type' => array(
          'air' => 'Air',
          'argon' => 'Argon',
          'krypton' => 'Krypton',
          'sf6' => 'SF6'
        )
      )
    ),
    'glass_data' => array(
      'double' => array(
        'double_glass1' => array(
          '4-6-4' => array(
            'air' => 3.3,
            'argon' => 3.0,
            'krypton' => 2.8,
            'sf6' => 3.0
          ),
          '4-9-4' => array(
            'air' => 3.0,
            'argon' => 2.8,
            'krypton' => 2.6,
            'sf6' => 3.1
          ),
          '4-12-4' => array(
            'air' => 2.9,
            'argon' => 2.7,
            'krypton' => 2.6,
            'sf6' => 3.1
          ),
          '4-15-4' => array(
            'air' => 2.7,
            'argon' => 2.7,
            'krypton' => 2.6,
            'sf6' => 3.1
          ),
          '4-20-4' => array(
            'air' => 2.7,
            'argon' => 2.6,
            'krypton' => 2.6,
            'sf6' => 3.1
          ),
        ),
        'double_glass2' => array(
          '4-6-4' => array(
            'air' => 2.9,
            'argon' => 2.6,
            'krypton' => 2.2,
            'sf6' => 2.6
          ),
          '4-9-4' => array(
            'air' => 2.6,
            'argon' => 2.3,
            'krypton' => 2.0,
            'sf6' => 2.7
          ),
          '4-12-4' => array(
            'air' => 2.4,
            'argon' => 2.1,
            'krypton' => 2.0,
            'sf6' => 2.7
          ),
          '4-15-4' => array(
            'air' => 2.2,
            'argon' => 2.0,
            'krypton' => 2.0,
            'sf6' => 2.7
          ),
          '4-20-4' => array(
            'air' => 2.2,
            'argon' => 2.0,
            'krypton' => 2.0,
            'sf6' => 2.7
          ),
        ),
        'double_glass3' => array(
          '4-6-4' => array(
            'air' => 2.7,
            'argon' => 2.3,
            'krypton' => 1.9,
            'sf6' => 2.3
          ),
          '4-9-4' => array(
            'air' => 2.3,
            'argon' => 2.0,
            'krypton' => 1.6,
            'sf6' => 2.4
          ),
          '4-12-4' => array(
            'air' => 1.9,
            'argon' => 1.7,
            'krypton' => 1.5,
            'sf6' => 2.4
          ),
          '4-15-4' => array(
            'air' => 1.8,
            'argon' => 1.6,
            'krypton' => 1.6,
            'sf6' => 2.5
          ),
          '4-20-4' => array(
            'air' => 1.8,
            'argon' => 1.7,
            'krypton' => 1.6,
            'sf6' => 2.5
          ),
        ),
        'double_glass4' => array(
          '4-6-4' => array(
            'air' => 2.6,
            'argon' => 2.2,
            'krypton' => 1.7,
            'sf6' => 2.1
          ),
          '4-9-4' => array(
            'air' => 2.1,
            'argon' => 1.7,
            'krypton' => 1.3,
            'sf6' => 2.2
          ),
          '4-12-4' => array(
            'air' => 1.8,
            'argon' => 1.5,
            'krypton' => 1.3,
            'sf6' => 2.3
          ),
          '4-15-4' => array(
            'air' => 1.6,
            'argon' => 1.4,
            'krypton' => 1.3,
            'sf6' => 2.3
          ),
          '4-20-4' => array(
            'air' => 1.6,
            'argon' => 1.4,
            'krypton' => 1.3,
            'sf6' => 2.3
          ),
        ),
        'double_glass5' => array(
        '4-6-4' => array(
          'air' => 2.5,
          'argon' => 2.1,
          'krypton' => 1.5,
          'sf6' => 2.0
        ),
        '4-9-4' => array(
          'air' => 2.0,
          'argon' => 1.6,
          'krypton' => 1.3,
          'sf6' => 2.1
        ),
        '4-12-4' => array(
          'air' => 1.7,
          'argon' => 1.3,
          'krypton' => 1.1,
          'sf6' => 2.2
        ),
        '4-15-4' => array(
          'air' => 1.5,
          'argon' => 1.2,
          'krypton' => 1.1,
          'sf6' => 2.2
        ),
        '4-20-4' => array(
          'air' => 1.5,
          'argon' => 1.2,
          'krypton' => 1.2,
          'sf6' => 2.2
        ),
      ),
      ),
      'triple' => array(
        'triple_glass1' => array(
          '4-6-4-6-4' => array(
            'air' => 2.3,
            'argon' => 2.1,
            'krypton' => 1.8,
            'sf6' => 2.0
          ),
          '4-9-4-9-4' => array(
            'air' => 2.0,
            'argon' => 1.9,
            'krypton' => 1.7,
            'sf6' => 2.0
          ),
          '4-12-4-12-4' => array(
            'air' => 1.9,
            'argon' => 1.8,
            'krypton' => 1.6,
            'sf6' => 2.0
          )
        ),
        'triple_glass2' => array(
          '4-6-4-6-4' => array(
            'air' => 2.0,
            'argon' => 1.7,
            'krypton' => 1.4,
            'sf6' => 1.6
          ),
          '4-9-4-9-4' => array(
            'air' => 1.7,
            'argon' => 1.5,
            'krypton' => 1.2,
            'sf6' => 1.6
          ),
          '4-12-4-12-4' => array(
            'air' => 1.8,
            'argon' => 1.5,
            'krypton' => 1.1,
            'sf6' => 1.3
          )
        ),
        'triple_glass3' => array(
          '4-6-4-6-4' => array(
            'air' => 1.8,
            'argon' => 1.5,
            'krypton' => 1.1,
            'sf6' => 1.3
          ),
          '4-9-4-9-4' => array(
            'air' => 1.4,
            'argon' => 1.2,
            'krypton' => 0.9,
            'sf6' => 1.3
          ),
          '4-12-4-12-4' => array(
            'air' => 1.2,
            'argon' => 1.0,
            'krypton' => 0.8,
            'sf6' => 1.4
          )
        ),
        'triple_glass4' => array(
          '4-6-4-6-4' => array(
            'air' => 1.7,
            'argon' => 1.3,
            'krypton' => 1.0,
            'sf6' => 1.2
          ),
          '4-9-4-9-4' => array(
            'air' => 1.3,
            'argon' => 1.0,
            'krypton' => 0.8,
            'sf6' => 1.2
          ),
          '4-12-4-12-4' => array(
            'air' => 1.1,
            'argon' => 0.9,
            'krypton' => 0.6,
            'sf6' => 1.2
          )
        ),
        'triple_glass5' => array(
          '4-6-4-6-4' => array(
            'air' => 1.6,
            'argon' => 1.3,
            'krypton' => 0.9,
            'sf6' => 1.1
          ),
          '4-9-4-9-4' => array(
            'air' => 1.2,
            'argon' => 0.9,
            'krypton' => 0.7,
            'sf6' => 1.1
          ),
          '4-12-4-12-4' => array(
            'air' => 1.0,
            'argon' => 0.8,
            'krypton' => 0.5,
            'sf6' => 1.1
          )
        ),
      )
    ),
  );
  echo json_encode($data);
